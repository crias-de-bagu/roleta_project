from django.contrib import admin
from main import views
from django.urls import path


app_name = 'CassinoRoyale'

urlpatterns = [
    path(
        'me/wallet/',
        views.wallet_detail,
        name='my-wallet'
    ),
    
    path(
        'me/movement/',
        view=views.MovementListView.as_view(),
        name='my-movement-list'
    ),

    path(
        'roleta/',
        views.roleta_detail,
        name='roleta-inicial'
    ),
]
